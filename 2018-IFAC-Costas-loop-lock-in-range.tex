\documentclass{ifacconf}

\usepackage{graphicx}
\usepackage{natbib}

\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{subcaption}

\usepackage[update,prepend,suffix=]{epstopdf}

% \usepackage[symbol]{footmisc}

% \usepackage{amsthm} % Already in IFAC .sty
\newtheorem{example}{Example}
\newtheorem{lemma}{Lemma}
\newtheorem{theorem}{Theorem}
\newtheorem{remark}{Remark}
\newtheorem{definition}{Definition}
\newtheorem{proposition}{Assertion}

\begin{document}
\begin{frontmatter}

\title{Lock-in range of BPSK Costas loop}

\thanks[]{This work was supported by Russian Scientific Foundation (project 14-21-00041).}


\author[spb]{K.D.~Aleksandrov},
\author[spb]{M.S.~Krasnikova},
\author[spb,ipmash,fin]{N.V.~Kuznetsov},
\author[]{G.A.~Leonov$^\dagger$}
\address[spb]{Faculty of Mathematics and Mechanics,\
 Saint-Petersburg State University, Russia}
\address[fin]{Dept. of Mathematical Information Technology,\
 University of Jyv\"{a}skyl\"{a}, Finland}
\address[ipmash]{Institute of Problems of Mechanical Engineering RAS, Russia \\  email: nkuznetsov239@gmail.com, k.aleksandrov.239@gmail.com}

\begin{abstract}
\hskip0.3cm 
This work is devoted to nonlinear analysis of BPSK Costas loops.
Key characteristics, such as the hold-in, pull-in, and lock-in ranges, are estimated.
It is shown that obtained estimations refine some previously known engineering results.
\end{abstract}

\begin{keyword}
BPSK Costas loop, PI filter, lock-in range, nonlinear analysis, numerical simulations
\end{keyword}

\end{frontmatter}

\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\footnotetext[5]{Deceased 23 April 2018.}
\renewcommand{\thefootnote}{\arabic{footnote}}


\section{Introduction}
Costas loop is a classical phase-locked loop (PLL) based circuit for carrier recovery and signal demodulation \citep{Costas-1956,Costas-1962-patent}. PLL-based circuits are automatic control system, which are designed to generate an electrical signal (voltage), the frequency of which is automatically tuned to the frequency of the input (reference) signal. Nowadays Costas loops are widely used in Global Positioning Systems, wireless communications and other applications
(see, e.g., \citep{BestKLYY-2016,Best-2018}).

In the present paper a \textit{nonlinear model in the signal's phase space} of classical BPSK Costas loop is studied and its key parameter characteristics -- the hold-in, pull-in, and lock-in ranges -- are discussed. Numerical-analytical approach of the lock-in range estimation is applied to the considered model of classical BPSK Costas loop. The results presented in the paper are compared with known results for modified BPSK Costas loop.

\section{Problem statement}

Let us describe the operation principle of classical BPSK Costas loop. The loop contains the following components (see Fig.~\ref{ris:classicalCostasLoop}): voltage-controlled oscillator (VCO), three multipliers, $90^\circ$ block, low-pass side filters (LPF1 and LPF2), and filter (LF).
\begin{figure}[!htbp]
\centering
\includegraphics[width=\linewidth]{Pics/CostasClassicalSS}
\caption{Classical BPSK Costas loop}
\label{ris:classicalCostasLoop}
\end{figure}
\begin{figure}[!htbp]
\centering
\includegraphics[width=\linewidth]{Pics/CostasModifiedSS}
\caption{Modified BPSK Costas loop}
\label{ris:modifiedCostasLoop}
\end{figure}

The loop operates to adjust the frequency of VCO $\omega_{\rm vco}(t)$ to the carrier frequency $\omega_{\rm ref}$ \footnote{The carrier frequency is usually assumed to be constant.}.
The input of the circuit (Input) is the product of data signal $m(t) = \{+1, -1\}$ and carrier $\sin \theta_{\rm ref} (t)$. The signal $\sin \theta_{\rm vco} (t)$ is produced by voltage-controlled oscillator (VCO), which free-running frequency is denoted by $\omega_{\rm vco}^\text{free}$. Further the designations $\theta_e(t)$ and $\omega_e^\text{free}$ are used for $(\theta_{\rm ref}(t) - \theta_{\rm vco}(t))$ and $(\omega_{\rm ref} - \omega_{\rm vco}^\text{free})$, respectively. The $90^\circ$ block shifts the phase of VCO signal by $\pi / 2$.

Variation in $m(t)$ during transient processes may lead to undesirable effects such as false-locking (see, e.g., \citep{Olson-1975,Simon-1978,Stensby-1989,BestKKLYY-2015-ACC}). To avoid those effects one may try either to design the loop with synchronization time being less than the time between changes of $m(t)$ (by choosing corresponding loop parameters) or to modify the loop (see, e.g., \citep{Olson-1975}). Thus, we assume that $m(t) \equiv {\rm const}$ during the transient processes and consider $m(t) \equiv 1$ further.

Here we consider low-pass filter LPF1 (and LPF2 as well) with transfer function $H_{\rm LPF}(s) = \frac{1}{1 +  s / \omega_3}$, $\omega_3 > 0$. The output signal of LPF1 is denoted by $x_1(t)$, and $x_1(0)$ is the initial state of filter LPF1 (index '$_2$' is used for LPF2). Also, we consider LF being the PI (proportional+integral) filter with transfer function $H_{\rm LF} = \frac{\tau_2 s + 1}{\tau_1 s}$, $\tau_1 > 0$, $\tau_2 > 0$; $x(t)$ is the state of filter LF.

The following \textit{nonlinear mathematical model in the signal space} corresponds to the classical BPSK Costas loop under consideration (see, e.g., \citep{LeonovKYY-2015-DAN-BPSK,BestKLYY-2016}):
\begin{equation}
\label{sys:classicalSysT}
\begin{cases}
\dot{x}_1(t) = \omega_3 \left( - x_1(t) + \frac{\cos\theta_e(t) - \cos(\theta_{\rm ref}(t)+\theta_{\rm vco}(t))}{2}\right), \\
\dot{x}_2(t) = \omega_3 \left( - x_2(t) + \frac{\sin\theta_e(t) + \sin(\theta_{\rm ref}(t)+\theta_{\rm vco}(t))}{2}\right), \\
\dot{x}(t) = \frac{1}{\tau_1} x_1(t) x_2(t), \\
\dot{\theta}_e (t)= \omega_e^\text{free} - K_{\rm vco} \left(x(t) + \frac{\tau_2}{\tau_1}x_1(t) x_2(t) \right).
\end{cases}
\end{equation}

%\begin{figure}[!htbp]
%\centering
%\includegraphics[width=\linewidth]{Pics/CostasClassicalSPS}
%\caption{The operating principle of model \eqref{sys:classicalSys}}
%\label{ris:classicalCostasLoopSPS}
%\end{figure}
However, the study of acquisition processes for \textit{signal space model} of BPSK Costas loop is difficult, since the equations describing the model are nonautonomous. For this reason the \textit{signal's phase space models} are widely used nowadays for the study of acquisition processes in PLL-based circuits.
In \citep{LeonovKYY-2015-DAN-BPSK,LeonovKYY-2015-SIGPRO} the signal space model \eqref{sys:classicalSysT} was rigorously reduced\footnote{
Remark that the rigorous reduction of \textit{signal space models} to \textit{signal's phase space models} is often omitted (see, e.g. classical books \cite[p.12,15-17]{Viterbi-1966}, \cite[p.7]{Gardner-1966}).
However, such reduction is valid under certain conditions,
which violation may lead to unreliable results
(see, e.g. \citep{KuznetsovKLNYY-2015-ISCAS,BestKKLYY-2015-ACC}).
}
to the signal's phase space model
\begin{equation}
\label{sys:classicalSys}
\begin{cases}
\dot{x}_1 = \omega_3 \left( - x_1 + \frac{1}{2}\cos\theta_e \right), \\
\dot{x}_2 = \omega_3 \left( - x_2 + \frac{1}{2}\sin\theta_e \right), \\
\dot{x} = \frac{1}{\tau_1} x_1 x_2, \\
\dot{\theta}_e = \omega_e^\text{free} - K_{\rm vco} \left(x + \frac{\tau_2}{\tau_1}x_1 x_2 \right).
\end{cases}
\end{equation}
The averaging methods \citep{MitropolskyB-1961,Samoilenko-2004-averiging} were used to perform the reduction. %The model \eqref{sys:classicalSys} may be represented by circuit in Fig.~\ref{ris:classicalCostasLoopSPS}.


In this paper the operating ranges of classical BPSK Costas loop,
given by model \eqref{sys:classicalSys},
are estimated and compared with corresponding ranges of
\textit{modified} BPSK Costas loop (see Fig.~\ref{ris:modifiedCostasLoop}).
%, which was studied, e.g., in \citep{KaplanH-2006-GPS,AlexandrovKLNS-2015-IFAC,BestKLYY-2016}.

% \citep{Rohde-2000-book,KaplanH-2006-GPS,BestKLYY-2014-IJAC,BestKLYY-2014-DCNPS,AlexandrovKLNS-2015-IFAC,BestKLYY-2016}.


%\begin{figure}[!htbp]
%\centering
%\includegraphics[width=\linewidth]{Pics/CostasModifiedSPS}
%\caption{The operating principle of model \eqref{sys:modifiedSys}}
%\label{ris:modifiedCostasLoopSPS}
%\end{figure}
The difference between classical and modified BPSK Costas loops is that low-pass filters LPF1 and LPF2 are removed from the loop and, thus, do not affect the synchronization processes. The model of modified BPSK Costas loop in the signal's phase space %corresponds to circuit in Fig.~\ref{ris:modifiedCostasLoopSPS} and
is described by the following relations \citep{BestKKLYY-2015-ACC}:
\begin{equation}
\label{sys:modifiedSys}
\begin{cases}
\dot{x} = \frac{1}{8\tau_1}\sin 2\theta_e, \\
\dot{\theta}_\Delta = \omega_e^\text{free} - K_{\rm vco} \left( x + \frac{\tau_2}{8 \tau_1}\sin 2\theta_e \right).
\end{cases}
\end{equation}

Note that \eqref{sys:classicalSys} is not changed by the transformation
\begin{align*}
& (\omega_e^\text{free}, x_1(t), x_2(t), x(t), \theta_e(t)) \to  \\
& \hskip0.5cm \to (-\omega_e^\text{free}, x_1(t), -x_2(t), -x(t), -\theta_e(t)),
\end{align*}
as well \eqref{sys:modifiedSys} is not changed by the transformation
$$\left(\omega_e^{\rm free}, x(t), \theta_e(t) \right) \to \left(-\omega_e^{\rm free}, -x(t), -\theta_e(t) \right).$$
This fact allows the consideration of models \eqref{sys:classicalSys}, \eqref{sys:modifiedSys} with only $\omega_e^{\text{free}}>0$
and introduces the concept of \textit{frequency deviation}: $$|\omega_e^{\text{free}}| = |\omega_{\rm{ref}} - \omega_{\rm{vco}}^{\text{free}}|.$$

Due to different operation modes, which are desired in applications, several stability ranges of BPSK Costas loop are of interest. In the next sections we discuss \textit{hold-in}, \textit{pull-in}, and \textit{lock-in} ranges of classical BPSK Costas loop and compare obtained results with known results for modified BPSK Costas loop.




\section{The hold-in range}

%For nonlinear mathematical model in the signal's phase space \eqref{sys:classicalSys}
%we can consider the conditions of complete synchronization,
%i.e. the frequency error is zero and the phase difference $\theta_e(t)$ is constant:
%\begin{equation}
%\dot\theta_e(t)\equiv 0, \hskip0.5cm \theta_e(t) \equiv \theta^{eq}.
%\nonumber
%\end{equation}

%If these conditions hold, the filter states $x_1(t)$, $x_2(t)$, $x(t)$ tend to constant values $x_1^{eq}$, $x_2^{eq}$, $x^{eq}$ respectively. Thus,
The \textit{locked states} of BPSK Costas loop model in the signal's phase space are given by the equilibria ($x_1^{eq}$, $x_2^{eq}$, $x^{eq}$, $\theta^{eq}$) of system \eqref{sys:classicalSys}:
% , since the problems related to the loop synchronization time are not considered in this paper:
\begin{subequations}
\label{rel:classicalEquilibria}
\renewcommand{\arraystretch}{2}
\begin{eqnarray}
& (0.5, 0, \omega_e^\text{free} / K_{\rm vco}, 2\pi n), \label{equilibria_st1} \\
& (0, 0.5, \omega_e^\text{free} / K_{\rm vco}, \pi / 2 + 2\pi n), \label{equilibria_unst1} \\
& (-0.5, 0, \omega_e^\text{free} / K_{\rm vco}, \pi + 2\pi n), \label{equilibria_st2}\\
& (0, -0.5, \omega_e^\text{free} / K_{\rm vco}, 3\pi / 2 + 2\pi n), \label{equilibria_unst2}
\end{eqnarray}
\end{subequations}
where $n \in \mathbb {Z}.$
% Assume the loop is in a locked state. If the carrier frequency is being changed within the hold-in range slowly, the loop \textit{tracks} this change, i.e. the VCO frequency $\omega_{\rm vco} (t)$ changes following the carrier frequency $\omega_{\rm ref} (t)$.
Here we use the following rigorous definition for the hold-in range
\citep{KuznetsovLYY-2015-IFAC-Ranges,LeonovKYY-2015-TCAS,BestKLYY-2016}:
\begin{definition}
The largest interval $[0, \omega_h )$ of frequency deviations $|\omega_e^\text{free}|$, such that the loop reachieves locked state after small perturbations of the filters’ state, the phases and frequencies of VCO, and the input signals, is called a \textit{hold-in range} (in general the stable equilibria can be considered as a multiple- valued function, in which case the existence of its continuous single-value branch is required). Frequency deviation $\omega_h$ is called a \textit{hold-in frequency}.
\end{definition}




Thus, to find the hold-in range one needs to check if there exists any \textit{asymptotically stable} equilibrium among equilibria \eqref{rel:classicalEquilibria} of model \eqref{sys:classicalSys}.
To define the type of equilibria one needs to write out linearized system of \eqref{sys:classicalSys}, its characteristic polynomial, and check the eigenvalues in the neighborhood of each equilibrium.
%The characteristic polynomial takes the form:
%\begin{align*}
%&\chi(\lambda) = (\lambda + \omega_3)\Big(\lambda^3 + \omega_3\lambda^2 + \frac{\omega_3 \tau_2 K_{\rm vco}}{2\tau_1}(x_1^{eq}\cos \theta^{eq} - \\
%& - x_2^{eq} \sin \theta^{eq} )\lambda +  \frac{\omega_3 K_{\rm vco}}{2\tau_1}(x_1^{eq}\cos \theta^{eq}  - x_2^{eq} \sin \theta^{eq} ) \Big).
%\label{EQsysWith}
%\end{align*}

The application of Routh-Hurwitz criterion results in the following conditions for the stability of equilibrium:
\begin{equation}
\label{rel:holdInConditions}
\begin{cases}
\omega_3 >0, \\
\frac{K_{\rm vco} \omega_3 \tau_2}{2 \tau_1} \left( x_1^{eq} \cos \theta^{eq} - x_2^{eq} \sin \theta^{eq} \right) > 0, \\
\frac{K_{\rm vco} \omega_3}{2 \tau_1} \left( x_1^{eq} \cos \theta^{eq} - x_2^{eq} \sin \theta^{eq} \right) > 0, \\
\frac{K_{\rm vco} \omega_3}{2 \tau_1} \left(\omega_3 \tau_2 - 1\right) \left( x_1^{eq} \cos \theta^{eq} - x_2^{eq} \sin \theta^{eq} \right) > 0.
\end{cases}
\end{equation}
This implies that the equilibria \eqref{equilibria_unst1}, \eqref{equilibria_unst2} are unstable equilibria for each set of loop parameters and any frequency deviation. The remaining equilibria \eqref{equilibria_st1}, \eqref{equilibria_st2} are asymptotically stable for any frequency deviation if $\tau_2 > 1/ \omega_3$.

% Note that conditions \eqref{rel:holdInConditions} do not depend on $\omega_e^\text{free}$ and, consequently, the hold-in range of \eqref{sys:classicalSys} is either infinite or empty.
Thus, the following statement is valid:
\begin{lemma}
If condition $\tau_2 > 1/ \omega_3$ holds, the hold-in range of model \eqref{sys:classicalSys} is infinite: $\omega_h = + \infty$. Otherwise, the hold-in range of model \eqref{sys:classicalSys} is empty: $\omega_h = 0$.
\end{lemma}

This result on hold-in range differs from the hold-in range of modified BPSK Costas loop. The hold-in range of \eqref{sys:modifiedSys} is infinite for each set of loop parameters
% , since the equilibrium
% $$\left(\pi n, \omega_e^{\rm free} / K_{\rm vco}\right); \hskip0.2cm n \in \mathbb{Z}$$
%  of \eqref{sys:modifiedSys} is asymptotically stable, and the remaining equilibrium
% $$\left(\frac{\pi}{2} + \pi n, \omega_e^{\rm free} / K_{\rm vco} \right); \hskip0.2cm n \in \mathbb{Z}$$
% is unstable saddle equilibrium for any frequency deviation
(see, e.g., \citep{AlexandrovKLNS-2015-IFAC,AleksandrovKLNYY-2016-IFAC}).











\section{The pull-in and lock-in ranges}
\label{sec:LockIn}
%The other frequency ranges of PLL-based circuits, the pull-in range and the lock-in range, correspond to acquisition processes of these circuits. To study the ranges rigorously one needs to employ methods of nonlinear analysis, since the linear models describes the circuits' operation only in the neighborhood of locked states.

Frequency deviations for which the model in the signal's phase space achieves a locked state for any arbitrary initial state of its components\footnote{($x_1(0)$, $x_2(0)$, $x(0)$, $\theta_e(0)$) for model \eqref{sys:classicalSys} and ($x(0)$, $\theta_e(0)$) for model \eqref{sys:modifiedSys}, respectively} correspond to the \textit{pull-in range} $[0, \omega_p)$ (see, e.g., \citep{KuznetsovLYY-2015-IFAC-Ranges,LeonovKYY-2015-TCAS}). For the model \eqref{sys:modifiedSys} it is shown that its pull-in range is infinite (see, e.g., \cite{Bakaev-1963,LeonovA-DAN-2015}).
% For the model \eqref{sys:classicalSys} one can show that pull-in range can be estimated  by using the Lyapunov function for cylindrical phase space.


However, VCO frequency $\omega_{\rm vco} (t)$ may be slowly tuned to the carrier frequency $\omega_{\rm ref}$, and the acquisition process will take more than one beat note. Thus, the phase error $\theta_e (t)$ may substantially increase during the acquisition process.
To describe this effect rigorously, the notion of cycle slipping is used (see, e.g., \citep{AscheidM-1982,ErshovaL-1983}):
\begin{definition}
If \hskip0.2cm$\displaystyle \limsup_{t \to +\infty} \left|\theta_e(0) - \theta_e(t) \right| \geq \pi,$
it is said that cycle slipping\footnote{Here we give the definition of cycle slipping for model \eqref{sys:modifiedSys}, which is $\pi$-periodic in $\theta_e$. The model \eqref{sys:classicalSys} is $2\pi$-periodic in $\theta_e$, but there exist two asymptotically stable equilibria \eqref{equilibria_st1}, \eqref{equilibria_st2}. Since the multiple abrupt changes of frequency deviation may lead to the growth of phase error, it is reasonable to determine cycle slipping in model \eqref{sys:classicalSys} according to the given definition.} occurs.
\end{definition}

In order to consider the property of PLL-based circuit to synchronize without cycle slipping, a lock-in range concept was introduced in \citep{Gardner-1966}, and nowadays is widely used in engineering literature on the PLL-based circuits study (see, e.g., \citep{Best-1984,Wolaver-1991,KiharaOE-2002,Abramovitch-2002,Best-2007,Egan-2007-book,Baker-2011,Kroupa-2012,Middlestead-2017}).
In \citep{KuznetsovLYY-2015-IFAC-Ranges,LeonovKYY-2015-TCAS,BestKLYY-2016} it was suggested the following following rigorous\footnote{In 1979 Gardner wrote: \textit{``There is no natural way to define exactly any unique lock-in frequency''}, however \textit{``despite its vague reality, lock-in range is a useful concept''} \citep{Gardner-1979-book}.}
 definition of the lock-in range:
\begin{definition}
\label{def:LockIn}
The lock-in range is a largest interval of frequency deviations $\left|\omega_e^{\rm free}\right| \in \left[0, \omega_l\right)$ inside the pull-in range, such that after an abrupt change of $\omega_e^{\rm free}$ within the lock-in range the PLL reacquires lock without cycle slipping, if it is not interrupted. The frequency deviation $\omega_l$ is called a \textit{lock-in frequency}.
\end{definition}

Since the model \eqref{sys:classicalSys} has two stable equilibria \eqref{equilibria_st1}, \eqref{equilibria_st2}, one should consider both of them in order to determine the lock-in range.
Each of these equilibria has the maximum frequency deviation (namely, $\omega_{li}$, $i=1,2$) such that after any abrupt change of $\omega_e^{\rm free}$ within $(-\omega_{li}, \omega_{li})$ the loop acquires corresponding locked state
% (with the same coordinates in $x_1$, $x_2$, and $\theta_e$)
without cycle slipping.
Remark that \eqref{sys:classicalSys} is not changed by the transformation
\begin{align*}
&(\omega_e^\text{free}, x_1(t), x_2(t), x(t), \theta_e(t)) \to  \\
& \hskip0.5cm \to (-\omega_e^\text{free}, -x_1(t), x_2(t), -x(t), \pi-\theta_e(t)).
\end{align*}
Thus, $\omega_{l1} = \omega_{l2} = \omega_{l}$, and further we consider only one stable equilibrium $(0.5, 0, \omega_e^\text{free} / K_{\rm vco}, 2 \pi n)$ and corresponding maximum frequency deviation $\omega_l$.

\begin{figure}[!htbp]
\centering
\includegraphics[width=\linewidth]{Pics/LockInProcedure}
\caption{The lock-in range estimation}
\label{ris:lockInProcedure}
\end{figure}


\begin{figure}[!htbp]
\centering
\includegraphics[width=\linewidth]{Pics/CompareCostas10000}
\caption{Lock-in ranges comparison for model \eqref{sys:classicalSys} with different values of $\omega_3$ and model \eqref{sys:modifiedSys}; case $\tau_2 = 1$}
\label{ris:modifiedClassicalComparation}
\end{figure}

\begin{figure*}[!htbp]
\centering
\includegraphics[width=1\textwidth]{Pics/compareBest}
\caption{Estimates of lock-in range based on linear and nonlinear models of classical BPSK Costas loop; case $\tau_2=1$. Red dot: for $K_{\rm vco} = 500$, $\tau_1 = 0.5$, $\omega_3 = 1000$ with initial data ($x_1^{eq}$, $x_2^{eq}$, $x^{eq}(\omega_e^\text{free})$, $\theta^{eq}$) the cycle slipping occurs during transient process, which corresponds to abrupt change of $\omega_e^\text{free}$ from $71$ to $-71$}
\label{ris:compareBest}
\end{figure*}

Consider the way to estimate $\omega_l$ for the models \eqref{sys:classicalSys} and \eqref{sys:modifiedSys} \citep{LeonovKYY-2015-TCAS,KuznetsovLYY-arxiv2017}. Without loss of generality we fix $\omega_{\rm vco}^\text{free}$ and vary $\omega_{\rm ref}$. First, we set frequency deviation $\left|\omega_e^{\rm free}\right| = 0$ and check whether the model is in a locked state. Next, we apply
sufficiently small frequency step%\footnote{The choice of $\Delta\omega$ determines the accuracy of lock-in frequency estimate.}
$\Delta \omega > 0$. Consider the next steps ($k = 0, 1, \dots $) of lock-in range estimation.
At the $k^\text{th}$ step we abruptly change the carrier frequency by $(-1)^{k} (2k+1) \Delta\omega$ (i.e. the carrier frequency becomes $\omega_{\rm ref} = \omega_{\rm vco}^\text{free} + (-1)^{k} (k+1) \Delta \omega$) and observe whether the corresponding transient process converges to a locked state without cycle slipping (see Fig.~\ref{ris:lockInProcedure}). We stop the iterations when cycle slipping is detected during transient process at some step, namely the $N^\text{th}$ step. The desired frequency deviation $\omega_l$ is approximated as follows:
$$N \Delta \omega < \omega_l \leq (N+1)\Delta \omega.$$

\section{Estimates of lock-in range}


% \begin{figure*}[!htbp]
% \centering
% \includegraphics[width=0.8\textwidth]{Pics/compareBest1}
% \caption{Estimates of lock-in range based on linear and nonlinear models of classical BPSK Costas loop; case $\tau_2=1$. Right plot: lock-in range diagrams. Left plot: for $K_{\rm vco} = 0.25$, $\tau_1 = 0.5$, $\omega_3 = 1000$ with initial data ($x_1^{eq}$, $x_2^{eq}$, $x^{eq}(\omega_e^\text{free})$, $\theta^{eq}$) no cycle slipping during transient process, which corresponds to abrupt change of $\omega_e^\text{free}$ from $0.075$ to $-0.075$}
% \label{ris:compareBest1}
% \end{figure*}
%% cycle slipping during transient process, which corresponds to abrupt change of $\omega_e^\text{free}$ from $0.075$ to $-0.075$

In this section we present and discuss the results of suggested lock-in range estimation (see Section \ref{sec:LockIn}), which is performed for model \eqref{sys:classicalSys}. Application of numerical simulations allows to detect the cycle slips occurrence.

Note that system  \eqref{sys:classicalSys} depends on coefficients ($K_{\rm vco}$, $\tau_1$, $\tau_2$, $\omega_3$).
Consider the transformation $x \to x / \tau_1$ of \eqref{sys:classicalSys}. The transformation does not affect the cycle slipping property of trajectories.
By this transformation we get coefficients of \eqref{sys:classicalSys} changed as follows: $\tau_1 \to 1$, $K_{\rm vco} \to K_{\rm vco} / \tau_1$.
%Let's consider the lock-in range
Therefore, consider the lock-in range depending on the parameters:
\begin{equation}
  \label{eq:LockInUniformClassical}
  \omega_l = \omega_l (K_{\rm vco}, \tau_1, \tau_2, \omega_3) =
  \omega_l (K_{\rm vco} / \tau_1, \tau_2, \omega_3).
\end{equation}
Next, we compare the estimate for model \eqref{sys:classicalSys} with lock-in range estimate for model \eqref{sys:modifiedSys} (see \citep{AleksandrovKLNYY-2016-IFAC,AleksandrovKLYY-2016-arXiv-sin}). For system \eqref{sys:modifiedSys} the analogous relation holds:
\begin{equation}
  \label{eq:LockInUniformModified}
  \omega_l (K_{\rm vco}, \tau_1, \tau_2) =
  \omega_l (K_{\rm vco} / \tau_1, \tau_2).
\end{equation}
In order to compare these frequency ranges, the following steps are performed. First, we fix the value of $\tau_2$. For this fixed value we plot $\omega_l$ of model \eqref{sys:modifiedSys} as a function of $K_{\rm vco} / \tau_1$. %(such numerical estimates for were obtained in \citep{AleksandrovKLNYY-2016-IFAC,AleksandrovKLYY-2016-arXiv-sin})
Next, for the same value of $\tau_2$ we take several values of $\omega_3$ and plot $\omega_l$ of model \eqref{sys:classicalSys} as a function of $K_{\rm vco} / \tau_1$ for these values%\footnote{Remark that low-pass filters with large $\omega_3$ are preferably used in applications.}
(see Fig.~\ref{ris:modifiedClassicalComparation}).
Such consideration is useful, since it illustrates the influence of LPF1 and LPF2 being embedded in the loop.

Finally, let us compare obtained estimate with analytical estimate of $\omega_l$ based on consideration of closed loop transfer function (linear model of classical BPSK Costas loop), which was presented in \citep{Best-2018}:
\begin{equation}
\label{rel:BestEst}
\omega_l = \frac{K_{\rm vco} \tau_2}{4 \tau_1}.
\end{equation}

In Fig.~\ref{ris:compareBest} two curves are presented: the curve, which shows estimate \eqref{rel:BestEst}, while the dashed curve correspond to numerical estimate obtained in this paper.

\noindent \textit{Example.} Consider the loop with parameters $\tau_2=1$, $\tau_1 = 0.5$, $K_{\rm vco} = 500$, $\omega_3 = 1000$. According to \eqref{rel:BestEst}, the corresponding lock-in frequency $\omega_l = 250$. However, the cycle slipping during transient process occurs even for abrupt change of $\omega_e^\text{free}$ from $71$ to $-71$ (see Fig.~\ref{ris:compareBest}).


The considered example indicates that the results obtained by application of linear analysis to PLL-based circuits (which are nonlinear objects) should be carefully verified in practice. In contrast, the nonlinear analysis of PLL-based circuits may allow one to avoid undesirable effects in such systems.


\section*{Acknowledgment}
The authors would like to thank Roland~E.~Best
for valuable discussion on the stability ranges.


% The authors would like to thank Roland~E.~Best,
% the founder of the Best Engineering Company, Oberwil, Switzerland
% and the author of the bestseller on PLL-based circuits
% for valuable discussion.

%================================================
% \bibliography{C:/Dropbox/bib/bib_nk,C:/Dropbox/bib/bib_leonov,C:/Dropbox/bib/bib_full,C:/Dropbox/bib/bib_pll,C:/Dropbox/bib/bib-hidden}
\bibliography{D:/Dropbox/bib/bib_nk,D:/Dropbox/bib/bib_leonov,D:/Dropbox/bib/bib_full,D:/Dropbox/bib/bib_pll,D:/Dropbox/bib/bib-hidden}


\end{document} 